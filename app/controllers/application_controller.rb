class ApplicationController < ActionController::Base

  include ApplicationHelper

  protect_from_forgery with: :exception
  helper_method :period

  def index
    @model = ApplicationRecord.new(:time_period => 'current_week')
  end

  def period
    TimeperiodDecorator.new(@model)
  end

end
