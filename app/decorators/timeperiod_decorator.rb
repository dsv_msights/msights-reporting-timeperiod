class TimeperiodDecorator < Draper::Decorator
  attr_reader :time_period, :fiscal_year, :broadcast_year, :today
  delegate_all

  require "#{Rails.root}/lib/utilis/broadcast"
  require "#{Rails.root}/lib/utilis/fiscalyear"

  def initialize(object, options = {})
    super(object)
    @periods        = options[:periods] || get_default_periods
    @time_period    = options[:time_period] || object.time_period
    @start_year     = options[:start_year] || 1
    @fiscal_year    = options[:fiscal_year] || get_start_of_fiscal_year
    @broadcast_year = options[:broadcast_year] || get_start_of_broadcast_year
    @today          = options[:today] || Date.today
  end

  def valid?
    object.valid? && valid_time_period? || false
  end

  {:normalized_period_to => 'end_of', :normalized_period_from => 'beginning_of'}.each do |method_name, date_period_prefix|
    define_method "#{method_name}" do
      time, period_type = @time_period.split("_", 2)
      callback = "#{date_period_prefix}_#{period_type}"
      case time
      when 'custom'
        period = object.send(method_name.to_s.sub('normalized_',''))
        date = period.kind_of?(String) ? Date.parse(period) : period
        date.to_s
      when 'current'
        resolve_current_period(@today, period_type, callback)
      when 'last'
        resolve_last_period(@today, period_type, callback)
      end
    end
  end

  def update(attrs)
    attrs.each do |key, value|
      instance_variable_set("@#{key}", value) if defined?(key)
    end
  end

  private

  def resolve_current_period(date, period_type, callback)
    case period_type
    when 'fiscal_year', 'fiscal_quarter'
      current_fiscal_period(date, callback)
    when 'broadcast_year', 'broadcast_quarter', 'broadcast_month'
      current_broadcast_period(date, callback)
    else
      current_period(date, callback)
    end
  end

  def resolve_last_period(date, period_type, callback)
    case period_type
    when 'fiscal_year', 'fiscal_quarter'
      date = date.send("last_#{period_type}", @fiscal_year)
      last_fiscal_period(date, callback)
    when 'broadcast_year', 'broadcast_quarter', 'broadcast_month'
      args = @time_period.to_s.include?('month') ? nil : @broadcast_year
      date = date.send("prev_#{period_type}", *args)
      last_broadcast_period(date,callback)
    else
      date = date.send("last_#{period_type}")
      last_period(date, callback)
    end
  end

  [:current_period, :last_period].each do |method|
    define_method "#{method}" do |date, callback|
      date.send(callback).to_s
    end
  end

  [:current_fiscal_period, :last_fiscal_period].each do |method|
    define_method "#{method}" do |date, callback|
      date.send(callback, @fiscal_year).to_s
    end
  end

  [:current_broadcast_period, :last_broadcast_period].each do |method|
    define_method "#{method}" do |date, callback|
      args = @time_period.to_s.include?('month') ? nil : @broadcast_year
      date.send(callback, *args).to_s
    end
  end

  {:get_start_of_broadcast_year => :broadcast_year, :get_start_of_fiscal_year => :fiscal_year}.each do |method, key|
    define_method "#{method}" do
      if object.respond_to?(key) && !(object.send("#{key}").nil?)
        object.send("#{key}")
      else
        @start_year
      end
    end
  end

  def valid_time_period?
    if @time_period == 'custom'
      defined?(object.period_from) && defined?(object.period_to)
    else
      @periods.include?(@time_period)
    end
  end

  def get_default_periods
    [
      'current_week', 'current_month', 'current_quarter', 'current_year', 'current_fiscal_quarter', 'current_fiscal_year',
      'last_week', 'last_month', 'last_quarter', 'last_year', 'last_fiscal_quarter', 'last_fiscal_year',
      'current_broadcast_month', 'current_broadcast_quarter', 'current_broadcast_year', 'last_broadcast_month', 'last_broadcast_quarter', 'last_broadcast_year',
      'custom'
    ]
  end
end
