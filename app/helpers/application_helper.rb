module ApplicationHelper

  def get_type(period)
    if period.valid?
      period.time_period
    else
      'Period not valid'
    end
  end
end
