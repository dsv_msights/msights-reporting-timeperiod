class Date
  require "#{Rails.root}/lib/utilis/quarters"
  # Broadcast MONTH

  def beginning_of_broadcast_month(*args)
    if self > self.last_sunday_of_month
      beginning_of_week
    else
      beginning_of_month.beginning_of_week
    end
  end

  def end_of_broadcast_month(*args)
    if self > self.last_sunday_of_month
      next_week.last_sunday_of_month
    else
      last_sunday_of_month
    end
  end

  # Broadcast YEAR

  def beginning_of_broadcast_year(start_month = 1)
    date = self.end_of_broadcast_month

    if start_month <= date.month
      date.change(
        :month => start_month,
        :day   => 1
        ).beginning_of_broadcast_month
      else
        date.change(
        :year  => date.year - 1,
        :month => start_month,
        :day   => 1
        ).beginning_of_broadcast_month
      end
    end

    def end_of_broadcast_year(start_month = 1)
      (beginning_of_broadcast_year(start_month).end_of_broadcast_month + 11.months).last_sunday_of_month
    end

    # Broadcast QUARTER

    def beginning_of_broadcast_quarter(start_month = 1)
      t = self.beginning_of_broadcast_year(start_month)

      4.times do
        # if self >= t.beginning_of_broadcast_month && self <= (t.end_of_broadcast_month.beginning_of_month + 2.months).end_of_broadcast_month
        if self >= t.beginning_of_broadcast_month && self <= (t.end_of_broadcast_month + 2.months).last_sunday_of_month
          return t.beginning_of_broadcast_month
        else
          t = t.end_of_broadcast_month.beginning_of_month + 3.months
        end
      end
    end

    def end_of_broadcast_quarter(start_month = 1)
      t = self.beginning_of_broadcast_quarter(start_month)

      # (t.end_of_broadcast_month.beginning_of_month + 2.months).end_of_broadcast_month
      (t.end_of_broadcast_month + 2.months).last_sunday_of_month
    end

    # AGO

    def broadcast_months_ago(months, *args)
      end_of_broadcast_month.beginning_of_month.months_ago(months).beginning_of_broadcast_month
    end

    def broadcast_years_ago(years, start_month = 1)
      beginning_of_broadcast_year(start_month).end_of_broadcast_month.beginning_of_month.years_ago(years).beginning_of_broadcast_year(start_month)
    end

    def broadcast_quarters_ago(quarters, start_month = 1)
      beginning_of_broadcast_quarter(start_month).end_of_broadcast_month.beginning_of_month.quarters_ago(quarters).beginning_of_broadcast_quarter(start_month)
    end

    # PREV

    def prev_broadcast_month(*args)
      end_of_broadcast_month.beginning_of_month.prev_month.beginning_of_broadcast_month
    end

    def prev_broadcast_year(start_month = 1)
      beginning_of_broadcast_year(start_month).end_of_broadcast_month.beginning_of_month.prev_month.beginning_of_broadcast_year(start_month)
    end

    def prev_broadcast_quarter(start_month = 1)
      beginning_of_broadcast_quarter(start_month).end_of_broadcast_month.beginning_of_month.prev_month.beginning_of_broadcast_quarter(start_month)
    end

    # Utils

    def last_sunday_of_month
      date = self.end_of_month
      date -= date.wday
    end

    alias last_broadcast_year prev_broadcast_year
    alias last_broadcast_month prev_broadcast_month
    alias last_broadcast_quarter prev_broadcast_quarter
end
