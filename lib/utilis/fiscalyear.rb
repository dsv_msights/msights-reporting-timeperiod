class Date
  require "#{Rails.root}/lib/utilis/quarters"

  def beginning_of_fiscal_quarter(offset_month = 1)
    t = beginning_of_fiscal_year(offset_month)
    return t if self.month == offset_month
    4.times do
      return t if self >= t && self <= (t + 2.months).end_of_month
      t = t + 3.months
    end
  end

  def end_of_fiscal_quarter(offset_month = 1)
    (beginning_of_fiscal_quarter(offset_month) + 2.months).end_of_month
  end

  # Fiscal YEAR

  def beginning_of_fiscal_year(offset_month = 1)
    if offset_month <= self.month
      self.change(
        :month => offset_month,
        :day => 1
      ).beginning_of_month
    else
      self.change(
        :month => offset_month,
        :day => 1,
        :year => self.year - 1
      ).beginning_of_month
    end
  end

  def beginning_of_last_fiscal_year(offset_month = 1)
    beginning_of_fiscal_year(offset_month).last_year
  end

  def end_of_fiscal_year(offset_month = 1)
    (beginning_of_fiscal_year(offset_month) + 11.months).end_of_month
  end

  def end_of_last_fiscal_year(offset_month = 1)
    end_of_fiscal_year(offset_month).last_year
  end

  # PREV

  def prev_fiscal_quarter(offset_month = 1)
    beginning_of_fiscal_quarter(offset_month).months_ago(3)
  end

  def prev_fiscal_year(offset_month = 1)
    beginning_of_fiscal_year(offset_month).years_ago(1)
  end

  # AGO

  def fiscal_quarters_ago(quarters, offset_month = 1)
    quarters_ago(quarters).beginning_of_fiscal_quarter(offset_month)
  end

  def fiscal_years_ago(years, offset_month = 1)
    years_ago(years).beginning_of_fiscal_year(offset_month)
  end

  alias last_fiscal_year prev_fiscal_year
  alias last_fiscal_quarter prev_fiscal_quarter
end
