class Date
  def quarters_ago(quarters)
    months_ago(quarters.to_i * 3)
  end
end
