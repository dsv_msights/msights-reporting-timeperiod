require 'test_helper'

class ApplicationControllerTest < ActionDispatch::IntegrationTest

  test "decorated helper" do
    get "/"
    assert_response :success
    assert_equal 'current_week', response.parsed_body.strip
  end

end
