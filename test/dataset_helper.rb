
def params(time_period)
  type, period = time_period.split("_", 2)
  properties = "#{Rails.root}/test/datasets/model/cases/#{type}.yaml"
  yaml = YAML.load(File.read(properties))
  unless period.nil?
    yaml["time_period"] = "#{type}_#{period}"
  end
  Hash[yaml.map{|(k,v)| [k.to_sym,v]}]
end

def project_properties
  project = "#{Rails.root}/test/datasets/content_decorator/project.yaml"
  yaml = YAML.load(File.read(project))
  Hash[yaml.map{|(k,v)| [k.to_sym,v]}]
end
