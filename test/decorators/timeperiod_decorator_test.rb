require 'test_helper'
require 'dataset_helper'
require 'date_helper'

class TimeperiodDecoratorTest < Draper::TestCase

  test "extended valid? method" do
    p = params('current_month')
    m = ApplicationRecord.new(p)
    d = TimeperiodDecorator.new(m)
    assert d.valid?
  end

  test "project fiscal/broadcast years" do
    p = params('custom')
    m = ApplicationRecord.new(p)
    d = TimeperiodDecorator.new(m)

    assert_equal p[:fiscal_year], d.fiscal_year, "Incorrect property: [fiscal_year]"
    assert_equal p[:broadcast_year], d.broadcast_year, "Incorrect property: [broadcast_year]"
  end

  [
    ['current_month', Date.today.beginning_of_month.to_s, Date.today.end_of_month.to_s],
    ['current_week',  Date.today.beginning_of_week.to_s,  Date.today.end_of_week.to_s],
    ['current_year',  Date.today.beginning_of_year.to_s,  Date.today.end_of_year.to_s],
    ['current_quarter', Date.today.beginning_of_quarter.to_s, Date.today.end_of_quarter.to_s],
    ['last_month', Date.today.last_month.beginning_of_month.to_s, Date.today.last_month.end_of_month.to_s],
    ['last_week', Date.today.last_week.beginning_of_week.to_s, Date.today.last_week.end_of_week.to_s],
    ['last_quarter', Date.today.last_quarter.beginning_of_quarter.to_s, Date.today.last_quarter.end_of_quarter.to_s],
    ['last_year', Date.today.last_year.beginning_of_year.to_s, Date.today.last_year.end_of_year.to_s],
  ].each do |current_type, period_from, period_to|
    test "Expecting time_period [#{current_type}]" do
      p = params(current_type)
      m = ApplicationRecord.new(p)
      d = TimeperiodDecorator.new(m)

      assert_equal period_from, d.normalized_period_from, "Incorrect Time: [period_from]"
      assert_equal period_to, d.normalized_period_to, "Incorrect Time: [period_to]"
    end
  end

  [
    ['current_fiscal_year',
      [
        {:today => '2014-12-31', :expected_from => '2014-01-01'},
        {:today => '2018-06-30', :expected_from => '2018-01-01'},
        {:today => '2018-06-30', :expected_from => '2017-07-01', :fiscal_year => 7},
        {:today => '2018-09-01', :expected_from => '2018-09-01', :fiscal_year => 9},
        {:today => '2017-10-31', :expected_from => '2016-11-01', :fiscal_year => 11},
        {:today => '2015-09-01', :expected_to => '2015-12-31'},
        {:today => '2016-01-01', :expected_to => '2016-12-31'},
        {:today => '2018-10-27', :expected_to => '2019-08-31', :fiscal_year => 9},
        {:today => '2016-02-29', :expected_to => '2016-09-30', :fiscal_year => 10},
        {:today => '2017-12-31', :expected_to => '2018-10-31', :fiscal_year => 11}
      ]
    ],
    ['current_fiscal_quarter',
      [
        {:today => '2015-06-30', :expected_from => '2015-04-01'},
        {:today => '2015-07-01', :expected_from => '2015-07-01'},
        {:today => '2016-01-31', :expected_from => '2015-11-01', :fiscal_year => 2},
        {:today => '2017-11-12', :expected_from => '2017-09-01', :fiscal_year => 9},
        {:today => '2017-10-31', :expected_from => '2017-08-01', :fiscal_year => 11},
        {:today => '2015-06-30', :expected_to => '2015-06-30'},
        {:today => '2017-10-01', :expected_to => '2017-12-31'},
        {:today => '2017-03-10', :expected_to => '2017-04-30', :fiscal_year => 2},
        {:today => '2018-06-01', :expected_to => '2018-08-31', :fiscal_year => 9},
        {:today => '2017-08-31', :expected_to => '2017-10-31', :fiscal_year => 11}
      ]
    ]
  ].each do |current_type, use_cases|
    test "Expecting time_period [#{current_type}]" do
      p = params(current_type)
      m = ApplicationRecord.new(p)
      d = TimeperiodDecorator.new(m)

      prefix, postfix = current_type.to_s.split("_", 2)

      use_cases.each do |options|
        custom_date = Date.parse(options[:today] || Date.today.to_s)
        custom_fiscal_year = options[:fiscal_year] || 1

        d.update({
          fiscal_year: custom_fiscal_year,
          today: custom_date
        })
        period_from = options[:expected_from] || custom_date.send("beginning_of_#{postfix}", d.fiscal_year).to_s
        period_to = options[:expected_to] || custom_date.send("end_of_#{postfix}", d.fiscal_year).to_s

        assert_equal period_from, d.normalized_period_from, "Incorrect Time: [period_from]"
        assert_equal period_to, d.normalized_period_to, "Incorrect Time: [period_to]"
      end
    end
  end

  [
    ['last_fiscal_year',
      [
        {:today => '2018-06-20', :expected_from => '2017-01-01'},
        {:today => '2017-01-01', :expected_from => '2016-01-01'},
        {:today => '2017-06-30', :expected_from => '2015-07-01', :fiscal_year => 7},
        {:today => '2018-09-01', :expected_from => '2017-09-01', :fiscal_year => 9},
        {:today => '2017-12-31', :expected_from => '2016-11-01', :fiscal_year => 11}
      ]
    ],
    ['last_fiscal_quarter',
      [
        {:today => '2015-06-30', :expected_from => '2015-01-01'},
        {:today => '2016-02-29', :expected_from => '2015-10-01'},
        {:today => '2017-08-31', :expected_from => '2017-04-01', :fiscal_year => 7},
        {:today => '2016-06-01', :expected_from => '2016-03-01', :fiscal_year => 9},
        {:today => '2017-04-16', :expected_from => '2016-12-01', :fiscal_year => 12}
      ]
    ]
  ].each do |current_type, use_cases|
    test "Expecting time_period [#{current_type}]" do
      p = params(current_type)
      m = ApplicationRecord.new(p)
      d = TimeperiodDecorator.new(m)

      prefix, postfix = current_type.to_s.split("_", 2)

      use_cases.each do |options|
        custom_date = Date.parse(options[:today] || Date.today.to_s)
        custom_fiscal_year = options[:fiscal_year] || 1

        d.update({
          fiscal_year: custom_fiscal_year,
          today: custom_date
        })

        period_from = options[:expected_from] || custom_date.send("#{current_type}", d.fiscal_year).send("beginning_of_#{postfix}", d.fiscal_year).to_s
        period_to = options[:expected_to] || custom_date.send("#{current_type}", d.fiscal_year).send("end_of_#{postfix}", d.fiscal_year).to_s

        assert_equal period_from, d.normalized_period_from, "Incorrect Time: [period_from]"
        assert_equal period_to, d.normalized_period_to, "Incorrect Time: [period_to]"
      end
    end
  end

  [
    ['current_broadcast_month',
      [
        {:today => '2015-01-25', :expected_from => '2014-12-29'},
        {:today => '2015-03-12', :expected_from => '2015-02-23'},
        {:today => '2015-06-20', :expected_from => '2015-06-01'},
        {:today => '2018-06-25', :expected_from => '2018-06-25'},
        {:today => '2018-06-30', :expected_from => '2018-06-25'},
        {:today => '2019-04-01', :expected_from => '2019-04-01'},
        {:today => '2020-09-01', :expected_from => '2020-08-31'},
        {:today => '2015-03-12', :expected_to => '2015-03-29'},
        {:today => '2015-12-20', :expected_to => '2015-12-27'},
        {:today => '2018-06-25', :expected_to => '2018-07-29'},
        {:today => '2019-04-01', :expected_to => '2019-04-28'},
        {:today => '2020-06-28', :expected_to => '2020-06-28'},
        {:today => '2020-10-14', :expected_to => '2020-10-25'},
      ]
    ],
    ['current_broadcast_quarter',
      [
        {:today => '2015-06-20', :expected_from => '2015-03-30'},
        {:today => '2015-03-29', :expected_from => '2014-12-29'},
        {:today => '2017-06-25', :expected_from => '2017-03-27', :broadcast_year => 7},
        {:today => '2018-06-25', :expected_from => '2018-06-25', :broadcast_year => 7},
        {:today => '2015-03-12', :expected_from => '2015-02-23', :broadcast_year => 9},
        {:today => '2016-07-01', :expected_from => '2016-05-30', :broadcast_year => 9},
        {:today => '2017-02-26', :expected_from => '2016-11-28', :broadcast_year => 9},
        {:today => '2016-05-16', :expected_from => '2016-02-29', :broadcast_year => 12},
        {:today => '2015-06-20', :expected_to => '2015-06-28'},
        {:today => '2015-03-29', :expected_to => '2015-03-29'},
        {:today => '2017-11-01', :expected_to => '2017-12-31', :broadcast_year => 7},
        {:today => '2017-05-28', :expected_to => '2017-05-28', :broadcast_year => 9},
        {:today => '2018-08-27', :expected_to => '2018-11-25', :broadcast_year => 9},
        {:today => '2018-05-28', :expected_to => '2018-08-26', :broadcast_year => 9},
        {:today => '2017-10-30', :expected_to => '2018-01-28', :broadcast_year => 11},
      ]
    ],
    ['current_broadcast_year',
      [
        {:today => '2015-06-20', :expected_from => '2014-12-29'},
        {:today => '2015-12-28', :expected_from => '2015-12-28'},
        {:today => '2018-01-01', :expected_from => '2018-01-01'},
        {:today => '2018-12-30', :expected_from => '2018-01-01'},
        {:today => '2017-06-26', :expected_from => '2017-06-26', :broadcast_year => 7},
        {:today => '2018-02-23', :expected_from => '2017-08-28', :broadcast_year => 9},
        {:today => '2017-10-29', :expected_from => '2016-10-31', :broadcast_year => 11},
        {:today => '2015-09-01', :expected_to => '2015-12-27'},
        {:today => '2016-12-25', :expected_to => '2016-12-25'},
        {:today => '2015-12-28', :expected_to => '2016-12-25'},
        {:today => '2018-06-20', :expected_to => '2018-12-30'},
        {:today => '2018-08-27', :expected_to => '2019-08-25', :broadcast_year => 9},
        {:today => '2016-02-29', :expected_to => '2016-08-28', :broadcast_year => 9},
        {:today => '2017-12-31', :expected_to => '2018-10-28', :broadcast_year => 11},
        {:today => '2018-08-12', :expected_to => '2018-10-28', :broadcast_year => 11},
      ]
    ],
  ].each do |current_type, use_cases|
    test "Expecting time_period #{current_type}" do
      p = params("#{current_type}")
      m = ApplicationRecord.new(p)
      d = TimeperiodDecorator.new(m)

      prefix, postfix = current_type.to_s.split("_", 2)

      use_cases.each do |options|
        custom_date = Date.parse(options[:today] || Date.today.to_s)
        custom_broadcast_year = options[:broadcast_year] || 1

        d.update({
          broadcast_year: custom_broadcast_year,
          today: custom_date
        })

        case current_type
        when /month/
          period_from = options[:expected_from] || custom_date.send("beginning_of_#{postfix}").to_s
          period_to = options[:expected_to] || custom_date.send("end_of_#{postfix}").to_s
        else
          period_from = options[:expected_from] || custom_date.send("beginning_of_#{postfix}", d.broadcast_year).to_s
          period_to = options[:expected_to] || custom_date.send("end_of_#{postfix}", d.broadcast_year).to_s
        end

        assert_equal period_from, d.normalized_period_from, "Incorrect Time: [period_from]"
        assert_equal period_to, d.normalized_period_to, "Incorrect Time: [period_to]"
      end
    end
  end

  [
    ['last_broadcast_month',
      [
        {:today => '2015-06-20', :expected_from => '2015-04-27'},
        {:today => '2016-12-26', :expected_from => '2016-11-28'},
        {:today => '2016-02-29', :expected_from => '2016-02-01'},
        {:today => '2017-04-30', :expected_from => '2017-02-27'}
      ]
    ],
    ['last_broadcast_quarter',
      [
        {:today => '2015-06-20', :expected_from => '2014-12-29'},
        {:today => '2016-09-25', :expected_from => '2016-03-28'},
        {:today => '2016-12-26', :expected_from => '2016-09-26'},
        {:today => '2016-05-31', :expected_from => '2016-02-01', :broadcast_year => 5},
        {:today => '2015-08-31', :expected_from => '2015-06-01', :broadcast_year => 6},
        {:today => '2017-08-27', :expected_from => '2017-02-27', :broadcast_year => 9},
        {:today => '2017-02-27', :expected_from => '2016-11-28', :broadcast_year => 9},
      ]
    ],
    ['last_broadcast_year',
      [
        {:today => '2015-06-20', :expected_from => '2013-12-30', :broadcast_year => 1},
        {:today => '2016-12-26', :expected_from => '2015-12-28', :broadcast_year => 1},
        {:today => '2017-12-31', :expected_from => '2015-12-28', :broadcast_year => 1},
        {:today => '2016-02-01', :expected_from => '2015-01-26', :broadcast_year => 2},
        {:today => '2017-08-27', :expected_from => '2015-08-31', :broadcast_year => 9},
        {:today => '2016-04-01', :expected_from => '2014-09-01', :broadcast_year => 9}
      ]
    ],
  ].each do |current_type, use_cases|
    test "Expecting time_period [#{current_type}]" do
      p = params(current_type)
      m = ApplicationRecord.new(p)
      d = TimeperiodDecorator.new(m)

      prefix, postfix = current_type.to_s.split("_", 2)

      use_cases.each do |options|
        custom_date = Date.parse(options[:today] || Date.today.to_s)
        custom_broadcast_year = options[:broadcast_year] || 1

        d.update({
          broadcast_year: custom_broadcast_year,
          today: custom_date
        })

        case current_type
        when /month/
          period_from = options[:expected_from] || custom_date.send("prev_#{postfix}").send("beginning_of_#{postfix}").to_s
          period_to = options[:expected_to] || custom_date.send("prev_#{postfix}").send("end_of_#{postfix}").to_s
        else
          period_from = custom_date.send("prev_#{postfix}", d.broadcast_year).send("beginning_of_#{postfix}", d.broadcast_year).to_s
          period_to = custom_date.send("prev_#{postfix}", d.broadcast_year).send("end_of_#{postfix}", d.broadcast_year).to_s
        end

        assert_equal period_from, d.normalized_period_from, "Incorrect Time: [period_from]"
        assert_equal period_to, d.normalized_period_to, "Incorrect Time: [period_to]"
      end
    end
  end

  test "time_period: custom" do
    p = params('custom')
    m = ApplicationRecord.new(p)
    d = TimeperiodDecorator.new(m)

    actual_from = d.normalized_period_from
    actual_to = d.normalized_period_to

    assert d.valid?
    assert_equal p[:period_from], actual_from, "Incorrect Time: [period_from]"
    assert_equal p[:period_to], actual_to, "Incorrect Time: [period_to]"
  end

end
